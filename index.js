const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const cors = require('cors');
const cars = require('./cars.json');
require('dotenv').config();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.options('*', cors());

app.get('/', (req, res) => {
    res.send('OK');
});

app.get('/api', verificaToken, (req, res) => {
    res.send(cars);
})

app.post('/login', (req, res) => {

    const nomeUtente = req.body.nomeUtente;

    const tokenAccesso = jwt.sign({nome: nomeUtente}, process.env.TOKEN_ACCESSO, { expiresIn : "1h"});

    res.json({ nome: nomeUtente, tokenAccesso: tokenAccesso });
});

function verificaToken(req, res, next) {

    const token = req.headers["x-access-token"];

    if (!token)  return res.status(403).send("Un Token è necessario per ACCEDERE");
    
    try {
        const tokenVerificato = jwt.verify(token, process.env.TOKEN_ACCESSO);
        req.user = tokenVerificato;
    }
    catch (err) {
        return res.status(401).send("Token Invalido");
    }
    return next();

}

app.listen(3000, () => { console.log("PORTA 3000. "); });