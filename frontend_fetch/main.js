const inputNome = document.getElementById('inputDati');
const btnInvio = document.getElementById('btnInvio');
const tabella = document.getElementById('tabellaAuto');
const displayToken = document.getElementById('displayToken');

let datiAPI = [];

function fetchToken(nomeInput) {

    let riceviToken = new Promise((resolve, reject) => {
        //Richesta POST per ricevere il Token JWT
        fetch('http://localhost:3000/login', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            body: JSON.stringify({ "nome": nomeInput })
        })
            .then(res => res.json())
            .then(token => {
                resolve(token.tokenAccesso) 
                if(displayToken.length != 20) displayToken.innerText = 'Token JWT Ricevuto: '
                   
              
                    displayToken.innerText += " " + token.tokenAccesso
                
                })
    });

    //Utilizzando la Promise aspetta di ricevere il token prima di fare la richiesta di GET
    riceviToken.then((tokenRicevuto) => {

        fetch('http://localhost:3000/api', {
            method: 'get',
            headers: {
                //Header x-access-token necessario per ricevere i dati usando il token JWT
                'x-access-token': tokenRicevuto
            }
        })
            .then(res => res.json())
            .then(datiRicevuti => { datiAPI = datiRicevuti; console.log(datiAPI); })
    })

}

function creaTabellaAuto(datiAPI) {

    let tabellaAuto = datiAPI.map((auto) => {
        return  "<tr>" +
            "<td>" + auto.Model_ID + "</td>" +
            "<td>" + auto.Model_Name + "</td>" +
            "</tr>"

    }).join("");

    tabella.innerHTML += tabellaAuto



}

btnInvio.addEventListener('click', (e) => {
    e.preventDefault();
    fetchToken(inputNome.value);
    
    setTimeout(() => {
        creaTabellaAuto(datiAPI)
    }, 200);
})


